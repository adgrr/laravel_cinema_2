<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists')->insert([[
            'name' => 'Francis Ford',
            'firstname' => 'Coppola',
            'birthdate' => 1939,
        ],[
            'name' => 'Nolan',
            'firstname' => 'Christopher',
            'birthdate' => 1970,
        ],[
            'name' => 'DiCaprio',
            'firstname' => 'Leonardo',
            'birthdate' => 1974,
        ],[
            'name' => 'Cameron',
            'firstname' => 'James',
            'birthdate' => 1954,
        ],[
            'name' => 'Spielberg',
            'firstname' => 'Steven',
            'birthdate' => 1946,
        ],[
            'name' => 'Chazelle',
            'firstname' => 'Damien',
            'birthdate' => 1985,
        ],[
            'name' => 'Miyazaki',
            'firstname' => 'Hayao',
            'birthdate' => 1941,
        ],[ 
            'name' => 'Worthington',
            'firstname' => 'Sam',
            'birthdate' => 1976,
        ],[ 
            'name' => 'Saldana',
            'firstname' => 'Zoe',
            'birthdate' => 1978,
        ],[ 
            'name' => 'Welsh',
            'firstname' => 'Pat',
            'birthdate' => 1915,
        ],[ 
            'name' => 'Thomas',
            'firstname' => 'Henry',
            'birthdate' => 1971,
        ],[ 
            'name' => 'Schwarzenegger',
            'firstname' => 'Arnold',
            'birthdate' => 1947,
        ],[ 
            'name' => 'Hamilton',
            'firstname' => 'Linda',
            'birthdate' => 1956,
        ],[ 
            'name' => 'Bale',
            'firstname' => 'Christian',
            'birthdate' => 1974,
        ],[ 
            'name' => 'Neeson',
            'firstname' => 'Liam',
            'birthdate' => 1952,
        ],[ 
            'name' => 'Ledger',
            'firstname' => 'Heath',
            'birthdate' => 1979,
        ],[ 
            'name' => 'Hathaway',
            'firstname' => 'Anne',
            'birthdate' => 1982,
        ],[ 
            'name' => 'MacKay',
            'firstname' => 'George',
            'birthdate' => 1992,
        ],[ 
            'name' => 'Chapman',
            'firstname' => 'Dean-Charles',
            'birthdate' => 1997,
        ]]);
    }
}
