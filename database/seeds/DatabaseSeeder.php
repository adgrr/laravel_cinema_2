<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArtistsTableSeeder::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(CinemasTableSeeder::class);
        $this->call(RoomsTableSeeder::class);

        // Tables de jointure
        $this->call(ArtistMovieTableSeeder::class);
        $this->call(MovieRoomTableSeeder::class); //Show

        // $this->call(UsersTableSeeder::class);
    }
}
