<?php

use Illuminate\Database\Seeder;

class ArtistMovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_movie')->insert([[
            'role_name' => 'Batman / Bruce Wayne',
            'movie_id' => 6,
            'artist_id' => 14,
        ],[ 
            'role_name' => 'Batman / Bruce Wayne',
            'movie_id' => 7,
            'artist_id' => 14,
        ],[ 
            'role_name' => 'Batman / Bruce Wayne',
            'movie_id' => 8,
            'artist_id' => 14,
        ],[ 
            'role_name' => 'Jake Sully ',
            'movie_id' => 1,
            'artist_id' => 8,
        ],[ 
            'role_name' => 'Neytiri',
            'movie_id' => 1,
            'artist_id' => 9,
        ],[ 
            'role_name' => 'T-800',
            'movie_id' => 3,
            'artist_id' => 12,
        ],[ 
            'role_name' => 'T-800',
            'movie_id' => 4,
            'artist_id' => 12,
        ],[ 
            'role_name' => 'T-800',
            'movie_id' => 5,
            'artist_id' => 12,
        ],[ 
            'role_name' => 'Sara Connor',
            'movie_id' => 3,
            'artist_id' => 13,
        ],[ 
            'role_name' => 'Sara Connor',
            'movie_id' => 4,
            'artist_id' => 13,
        ],[ 
            'role_name' => 'Sara Connor',
            'movie_id' => 5,
            'artist_id' => 13,
        ],[ 
            'role_name' => 'Ra\'s al Ghul',
            'movie_id' => 6,
            'artist_id' => 15,
        ]]);
    }
}
