<?php

use Illuminate\Database\Seeder;

class MovieRoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movie_room')->insert([[
            'date_hour_start' => '2020-03-09',
            'movie_id' => 1,
            'room_id' => 1,
        ],[ 
            'date_hour_start' => '2020-04-09',
            'movie_id' => 2,
            'room_id' => 3,
        ],[ 
            'date_hour_start' => '2020-05-10',
            'movie_id' => 7,
            'room_id' => 5,
        ],[ 
            'date_hour_start' => '2020-12-12',
            'movie_id' => 7,
            'room_id' => 5,
        ],[ 
            'date_hour_start' => '2020-05-12',
            'movie_id' => 7,
            'room_id' => 5,
        ],[ 
            'date_hour_start' => '2020-07-09',
            'movie_id' => 7,
            'room_id' => 5,
        ],[ 
            'date_hour_start' => '2020-08-10',
            'movie_id' => 7,
            'room_id' => 5,
        ]]);
    }
}
