<?php $__env->startSection('title', 'All movies'); ?>
<?php $__env->startSection('content'); ?>
<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">All movies</h1>

        <?php if(session('ok')): ?>
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                <?php echo e(session('ok')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <?php endif; ?>

        <table class="table table-hover table-striped table-sm">
            <thead>
                <tr>
                    <th scope="row"><?php echo e(__('Title')); ?></th>
                    <th><?php echo e(__('Year')); ?></th>
                    <th><?php echo e(__('Director')); ?></th>
                    <th><?php echo e(__('Poster')); ?></th>
                    <th><?php echo e(__('Actions')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $movies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $movie): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><a href="/movie/<?php echo e($movie->id); ?>"><?php echo e($movie->title); ?></td>
                    <td><?php echo e($movie->year); ?></td>
                    <td><?php echo e($movie->director->firstname); ?> <?php echo e($movie->director->name); ?></td>
                    <td><img class="img-thumbnail" style="width: 180px; height: 240px;" src="/uploads/posters/poster_<?php echo e($movie->id); ?>.jpeg"></td>

                    <td class="table-action">
                        <a type="button" href="<?php echo e(route('movie.edit', $movie->id)); ?>" class="btn btn-sm"
                            data-toggle="tooltip" title="<?php echo app('translator')->get('Edit movie'); ?> <?php echo e($movie->title); ?>">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                        <a type="button" href="<?php echo e(route('movie.destroy', $movie->id)); ?>"
                            class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip"
                            title="<?php echo app('translator')->get('Delete artist'); ?> <?php echo e($movie->title); ?>">
                            <i class="fas fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>

        <div class="center-element" style="display: flex; justify-content: center;">
            <?php echo e($movies->appends(request()->except('page'))->links()); ?>

        </div>
    </div>
</main>
<script>
    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
        })

        $(document).on('click', '.btn-delete', function(){

            let button = $(this);

            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE'
            }).done(function(){
                button.closest('tr').remove();
            });
            return false;
        });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/movies/index.blade.php ENDPATH**/ ?>