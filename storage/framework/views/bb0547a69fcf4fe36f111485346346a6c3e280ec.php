<?php $__env->startSection('title', 'Add new artist'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center; margin-top: 50px;">Add new artist</h1>
        
        <form method="POST" action="<?php echo e(route('artist.store')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>


            <div class="form-group">
                <label for="firstname">First name</label>
                <input type="text" name="firstname" id="firstname" class="form-control form-control-sm" value=""
                    required />
            </div>

            <div class="form-group">
                <label for="name">Last name</label>
                <input type="text" name="name" id="name" value="" class="form-control form-control-sm" required />
            </div>
            <div class="form-group">
                <label for="birthdate">Birthdate</label>
                <input type="number" name="birthdate" id="birthdate" value="" class="form-control form-control-sm"
                    required />
            </div>

            <div class="form-group">
                <label for="photo">Artist photo</label>
                <input class="form-control form-control-file form-control-sm" type="file" name="photo" id="photo">
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Create</button>
        </form>
    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/artists/create.blade.php ENDPATH**/ ?>