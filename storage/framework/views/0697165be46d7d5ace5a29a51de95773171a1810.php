<?php $__env->startSection('title', 'All artists'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">All artists</h1>

        <?php if(session('ok')): ?>
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                <?php echo e(session('ok')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <?php endif; ?>
        
        <table class="table table-hover table-striped table-sm">
            <thead>
                <tr>
                    <th scope="row"><?php echo e(__('Name')); ?></th>
                    <th><?php echo e(__('Firstname')); ?></th>
                    <th><?php echo e(__('Birthdate')); ?></th>
                    <th><?php echo e(__('Photo')); ?></th>
                    <th><?php echo e(__('Filmography')); ?></th>
                    <th><?php echo e(__('Actions')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><a href="/artist/<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></a></td>
                    <td><?php echo e($artist->firstname); ?></td>
                    <td><?php echo e($artist->birthdate); ?></td>
                    <td><img class="img-thumbnail" style="width: 180px; height: 240px;" src="/uploads/photos/photo_<?php echo e($artist->id); ?>.jpeg" alt="<?php echo e($artist->id); ?>"></td>
                    <td>
                        <ul>
                            
                        </ul>
                    </td>
                    <td class="table-action">
                        <a type="button" href="<?php echo e(route('artist.edit', $artist->id)); ?>" class="btn btn-sm"
                            data-toggle="tooltip" title="<?php echo app('translator')->get('Edit artist'); ?> <?php echo e($artist->name); ?>">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                        <a type="button" href="<?php echo e(route('artist.destroy', $artist->id)); ?>"
                            class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip"
                            title="<?php echo app('translator')->get('Delete artist'); ?> <?php echo e($artist->name); ?>">
                            <i class="fas fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="center-element" style="display: flex; justify-content: center;">
            <?php echo e($artists->appends(request()->except('page'))->links()); ?>

        </div>
        
    </div>
</main>


<script>
    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
        })

        $(document).on('click', '.btn-delete', function(){

            let button = $(this);

            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE'
            }).done(function(){
                button.closest('tr').remove();
            });
            return false;
        });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/andre/Desktop/PROJECTS/cinema-laravel/resources/views/artists/index.blade.php ENDPATH**/ ?>