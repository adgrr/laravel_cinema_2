<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model{
	use SoftDeletes;

	protected $fillable = [
		'title', 'year', 'artist_id',
	];
	
	// Liaison one_to_many avec la table artist
	public function director(){
		return $this->belongsTo(Artist::class, 'artist_id');
	}
	
	// Liaison many_to_many avec la table artist
	public function actors(){
		return $this->belongsToMany(Artist::class);
	}

	// Liaison many_to_many avec la table room
	public function is_projected(){
		return $this->belongsToMany(Room::class);
	}
}
