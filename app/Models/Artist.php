<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model{
	protected $fillable = [
		'name','firstname', 'birthdate'
	];
	
	// Liaison one_to_many avec la table movie (Un artiste peut être direceteur de plusieurs films)
	public function has_directed(){
		return $this->hasMany('App\Models\Movie');
	}
	
	// Liaison many_to_many avec la table room (Un artiste peut jouer dans plusieurs films)
	public function has_played(){
		return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
	}
}