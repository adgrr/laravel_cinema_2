@extends ('layouts/app')
@section('title', 'Cinema Corp.')
@section('content')

<main>
    <div class="container">
        <div class="row align-items-start" style="margin: 50px 0;">
            <div class="col-md-4">
                <h2>Last 10 movies</h2>
                {{-- @foreach($movies as $movie)
                    {{ $movie->title }}
                @endforeach --}}
                <ul class="list-group">
                    <li class="list-group-item">Avatar</li>
                    <li class="list-group-item">E.T.</li>
                    <li class="list-group-item">Terminator</li>
                    <li class="list-group-item">Terminator 2</li>
                    <li class="list-group-item">Terminator 3</li>
                    <li class="list-group-item">Batman 1 - Batman Begins</li>
                    <li class="list-group-item">Batman 2 - The Dark Knight</li>
                    <li class="list-group-item">Batman 3 - The Dark Knight Rises</li>
                    <li class="list-group-item">Mon voisin Totoro</li>
                    <li class="list-group-item">1917</li>
                  </ul>
            </div>
            <div class="col-md-4">
                <h2>Last 10 actors</h2>
                {{-- @foreach($actors as $actor)
                    {{ $actor->firstname }} {{ $actor->name }}
                @endforeach --}}
                <ul class="list-group">
                    <li class="list-group-item">Saldana Zoe</li>
                    <li class="list-group-item">Welsh Pat</li>
                    <li class="list-group-item">Thomas Henry</li>
                    <li class="list-group-item">Schwarzenegger Arnold</li>
                    <li class="list-group-item">Hamilton Linda</li>
                    <li class="list-group-item">Neeson Liam</li>
                    <li class="list-group-item">Ledger Heath</li>
                    <li class="list-group-item">Hathaway Anne</li>
                    <li class="list-group-item">MacKay George</li>
                    <li class="list-group-item">Chapman Dean-Charles</li>
                  </ul>
            </div>
            <div class="col-md-4">
                <h2>Last 10 shows</h2>
                {{-- @foreach($movies as $movie)
                    {{ $movie->pivot->date_hour_start }}
                @endforeach --}}
                <ul class="list-group">
                    <li class="list-group-item">2020-09-09</li>
                    <li class="list-group-item">2020-09-10</li>
                    <li class="list-group-item">2020-09-11</li>
                    <li class="list-group-item">2020-09-12</li>
                    <li class="list-group-item">2020-09-13</li>
                    <li class="list-group-item">2020-09-14</li>
                    <li class="list-group-item">2020-09-15</li>
                    <li class="list-group-item">2020-09-16</li>
                    <li class="list-group-item">2020-09-17</li>
                    <li class="list-group-item">2020-09-18</li>
                  </ul>
            </div>
        </div>
    </div>
</main>
@endsection