@extends('layouts.app')
@section('title', 'Add new cinema')
@section('content')

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center;  margin-top: 50px;">Add new movie</h1>

        <form method="POST" action="{{ route('movie.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" id="year" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="artist_id">Pour quel cinema ?</label>
                <select class="custom-select" name="artist_id" required>
                    @foreach($artists as $artist)
                        <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="poster">Poster movie</label>
                <input class="form-control form-control-file form-control-sm" type="file" name="poster" id="poster">
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Create</button>
        </form>
    </div>
</main>
@endsection