@extends('layouts.app')
@section('title', 'All movies')
@section('content')
<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">All movies</h1>

        @if (session('ok'))
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        <table class="table table-hover table-striped table-sm">
            <thead>
                <tr>
                    <th scope="row">{{ __('Title') }}</th>
                    <th>{{ __('Year') }}</th>
                    <th>{{ __('Director') }}</th>
                    <th>{{ __('Poster') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movies as $movie)
                <tr>
                    <td><a href="/movie/{{$movie->id}}">{{ $movie->title }}</td>
                    <td>{{ $movie->year }}</td>
                    <td>{{ $movie->director->firstname }} {{ $movie->director->name }}</td>
                    <td><img class="img-thumbnail" style="width: 180px; height: 240px;" src="/uploads/posters/poster_{{ $movie->id}}.jpeg"></td>

                    <td class="table-action">
                        <a type="button" href="{{ route('movie.edit', $movie->id) }}" class="btn btn-sm"
                            data-toggle="tooltip" title="@lang('Edit movie') {{ $movie->title }}">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                        <a type="button" href="{{ route('movie.destroy', $movie->id) }}"
                            class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip"
                            title="@lang('Delete artist') {{$movie->title}}">
                            <i class="fas fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="center-element" style="display: flex; justify-content: center;">
            {{ $movies->appends(request()->except('page'))->links() }}
        </div>
    </div>
</main>
<script>
    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
        })

        $(document).on('click', '.btn-delete', function(){

            let button = $(this);

            $.ajax({
                url: $(this).attr('href'),
                type: 'DELETE'
            }).done(function(){
                button.closest('tr').remove();
            });
            return false;
        });
</script>
@endsection