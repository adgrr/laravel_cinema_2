@extends('layouts.app')
@section('title', 'Specific movie')
@section('content')

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">{{ $movie->title }}</h1>

        <div class="card" style="width: 18rem; margin: 0 auto;">
            <img src="/uploads/posters/poster_{{ $movie->id}}.jpeg" class="card-img-top" alt="{{ $movie->title}}">
            <div class="card-body">
                <h5 class="card-title">{{ $movie->title }}</h5>
                <p class="card-text">
                    {{ $movie->year }} 
                    <br>
                    Directed by: {{ $movie->director->firstname }} {{ $movie->director->name }}
                </p>
            </div>
        </div>
    </div>
    </div>
</main>
@endsection