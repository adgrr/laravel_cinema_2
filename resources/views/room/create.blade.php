@extends('layouts.app')
@section('title', 'Add new room')
@section('content')

<main>
    <div class="container">
        <h1 class="display-6" style="text-align: center;  margin-top: 50px;">Add new room</h1>

        <form method="POST" action="{{ route('room.store') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="" class="form-control form-control-sm" placeholder="Ex: Salle 1" required />
            </div>

            <div class="form-group">
                <label for="capacity">Capacity</label>
                <input type="number" name="capacity" id="capacity" value="" class="form-control form-control-sm" required />
            </div>

            <div class="form-group">
                <label for="cinema_id">Pour quel cinema ?</label>
                <select class="custom-select" name="cinema_id" required>
                    @foreach($cinemas as $cinema)
                        <option value="{{ $cinema->id }}">{{ $cinema->name }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-dark" style="display: block; margin: 0 auto;">Create</button>
        </form>
    </div>
</main>
@endsection