@extends('layouts.app')
@section('title', 'Specific artist')
@section('content')

<main>
    <div class="container">

        <h1 class="display-6" style="text-align: center; margin: 50px 0;">{{ $artist->name }}</h1>

        <div class="card" style="width: 18rem; margin: 0 auto;">
            <img src="/uploads/photos/photo_{{ $artist->id}}.jpeg" class="card-img-top" alt="{{ $artist->firstname }} {{ $artist->name }}">
            <div class="card-body">
                <h5 class="card-title">{{ $artist->firstname }} {{ $artist->name }}</h5>
                <p class="card-text">
                    {{ $artist->birthdate }}
                </p>
            </div>
        </div>
    </div>
</main>
@endsection